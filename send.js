var admin = require("firebase-admin");

var serviceAccount = require("/mnt/kamlesh/Personal/Projects/Quiz/firebasepushnotification/flutternew-2a265-firebase-adminsdk-2s3v6-29f17506c7.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://flutternew-2a265.firebaseio.com"
});

var registrationToken = 'MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC/TuYSET2205k1\nv9qqzeycJBnJuGY89MKpJIXo5sfogetSIS2BCKWjaG9XSBRzOYag3kq/8ZtUKGVI\n9Q2QSSxQbm4QUVZo2MUVQf2//oimwQ/Qqm7jj4C24xjwTgvGGa1+N0viYh85dUGZ\nKbJiATPbabEyoyphSGiPlk9XuO7lTVTnxPW20Y70GgRLwM51HI/6UyyF/OVyasIt\ngka4RB/i0/FNoH7qq7SKQri7FO/eINUZVKVECTWT8lGy9jIzwNdjspTzpVAHq7D5\nq0GAe+zrrOcD3DFayWDHzX8PFP8sVK1ZJItdmL+uc7beBOpF/JC/8p8EqL/I5gAr\nFMJ9OAfjAgMBAAECggEABCUlfwa4m5mCHFQH3OORWOC/XkuMi42SGjAcZqt5cWQ/\nQQxO/Bi/3gaJQw7gwo5giGPIotgNUYl+iFIm9RVgzG1YHSpmAOeRfaBsaxiMxXAx\nc4f91xho5Y2OIMbQIn6z76QrFMCjhI8x2Ku6nb0A7qQfnsVCSdFT0qijF3d+VaGJ\nszs8oQyHMs4sQvQxz0FEW7PMi1ULT9bS83xWisLeQzKSvDFCuv6GiHbf4qyHFKDD\nKfKNAHuHDCP9DAMZ8cRApcImmnJO209HgtiROsTXaU6LhMtFUmXWa4qX+afeKnAx\nkP4XFxBZtQGrq9sFXwhy9tTI+G1JCZ6Vbw8/PLiqQQKBgQDuzUs3VM0sv7eR41T1\n2koL2K9epk7IxmLwLaPsg+LWbK8goNAWNNbC4YHYbVN1G2WN7PeN8TO8j45RWDpX\nPBpzEh+d68LTyi/A9o1c83Gz8p9InZhleo2iYp7ipTr5VL7z5iBWYYfc7t4K9V50\nwrtGksahAzJNNWzYVTq+Kdf6+QKBgQDNFfrZu/MILxbllcUg0RLEIw3D70Iv+7DI\nNVwvGXGwNdAYSgU+1sb3nD2Ra/f4B0RUYtwy5pHFhDQxZBTsHjHa8Sjk5Rs4KaDt\ndzQ77UwsU4/+bbG/UOF0l3pqnFvbmd0deNjsqfVYDCLj4ay6Xt7bQK/+giNBBAmN\nN81X+2pUuwKBgQDCxFXwjw24zbUfG9cTnjJ+bwC5pdLkFkpjshXctcSxNnuvlPmi\n4v22fM62gpRrEylFFFzkPDICPOPKnLcYp0C7UbDP0JZavrEsJOsvQK8gIWagh0jg\nm5AhPWZxXGo5KVGFA9CkI2/4ViprrayYdqrf6J5wfHbuWgCogPskVmR9oQKBgH1T\nbjz6ZxTcOeDIunNIcHBdf375yfeqlihyu0epA9mLuQPuz+/LrkEIQHOKiDkQr8fl\nteyBDBmopYB9SW/VL+A8uSHPg+e5QLd8vfXVGFSKKBdyD0n7chseqldV/6heCUqG\nXAGcBursV7HsafJmIUfY6vPbsRF9Fx0uOZUHdb4bAoGAHjLpasf8EfZCbhZGJdIh\n6Urkyk1tKtc6UsojLQjRQFM+v6kaCUAQtG+Sk6tn0fLdYS/iB5qKwsWyArcRSrwG\nkcFUKxlyrcv04JeM+RQE6SXuuDma4DLpi8b4ac3ulzH75NqVuXGuzvjlQ+UeRZ7h\nqAATck1toCZnAJEC0mE0v2M=';

// See documentation on defining a message payload.
var message = {
  data: {
    score: '850',
    time: '2:45'
  },
  token: registrationToken
};
var option = {
  data: {
    priority : "high",
    timeToLive : 60*60*24
  }
};

// Send a message to the device corresponding to the provided
// registration token.
//admin.messaging().sendToDevice(registrationToken,message,option)
admin.messaging().send(message)
  .then((response) => {
    // Response is a message ID string.
    console.log('Successfully sent message:', response);
  })
  .catch((error) => {
    console.log('Error sending message:', error);
  });